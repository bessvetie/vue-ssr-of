const express = require('express');
const consola = require('consola');
const BodyParser = require('body-parser');
const { Nuxt, Builder } = require('nuxt');
const app = express();
const host = process.env.HOST || '127.0.0.1';
const port = process.env.PORT || 8081;

const PouchDB = require('pouchdb');
const pouchdbFind = require('pouchdb-find');
PouchDB.plugin(pouchdbFind);

const dbname = 'words';
const username = '9da925a0-05a2-4ce5-bf37-d3f66955987a-bluemix';
const password = '6d5bcd26618d0dc98e465700abb7f47cfdbbbee3588ea2d169015a91b9c178a2';
const database = new PouchDB(`https://${username}:${password}@${username}.cloudant.com/${dbname}/`);
const apiWords = require('./ApiWords')(database);
consola.info(`https://${username}:${password}@${username}.cloudant.com/${dbname}/`);

app.set('port', port);

app.use(BodyParser.json());
app.use(BodyParser.urlencoded({extended: true}));

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js');
config.dev = !(process.env.NODE_ENV === 'production');

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config);

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt);
    await builder.build();
  }

  // api
  app.get('/api/words', apiWords.get);

  // Give nuxt middleware to express
  app.use(nuxt.render);

  // Listen the server
  app.listen(port, host);
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  });
}
start();
