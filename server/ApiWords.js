module.exports = function(database) {
  return {
    get(req, res) {
      database.createIndex({index: {fields: ['type']}}).then(() => {
        // load all 'word' items
        const q = {
          selector: {
            type: 'word'
          }
        };
        return database.find(q);
      }).then(function(data) {
        res.send(data.docs);
      }, function(error) {
        res.status(400).send(error);
      });
    }
  };
};
