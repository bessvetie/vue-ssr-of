import Vue from 'vue';
import { detect } from 'detect-browser';
const browser = detect();

Vue.prototype.$isSupportedBrowser = () =>
  browser.name !== 'ie' || parseInt(browser.version) > 9;
