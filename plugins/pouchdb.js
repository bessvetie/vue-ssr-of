import Vue from 'vue';
import PouchDB from 'pouchdb';
import pouchdbFind from 'pouchdb-find';
PouchDB.plugin(pouchdbFind);

Vue.prototype.$pouchdbApi = {
  databases: {}
};

const newDataBase = ({dbname, username, password}) => {
  Vue.prototype.$pouchdbApi.databases[dbname] = {
    db:  new PouchDB(dbname),
    url: `https://${username}:${password}@${username}.cloudant.com/${dbname}/`
  };
};
newDataBase({
  dbname: 'words',
  username: '9da925a0-05a2-4ce5-bf37-d3f66955987a-bluemix',
  password: '6d5bcd26618d0dc98e465700abb7f47cfdbbbee3588ea2d169015a91b9c178a2'
});
