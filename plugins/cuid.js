import Vue from 'vue';
import cuid from 'cuid';

Vue.prototype.$cuid = cuid;
