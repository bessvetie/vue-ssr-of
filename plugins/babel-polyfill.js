import Vue from 'vue';
import BabelPolyfill from 'babel-polyfill';

Vue.use(BabelPolyfill);
