const pkg = require('./package');
const host = process.env.HOST || '127.0.0.1';
const port = process.env.PORT || 8081;

module.exports = {
  mode: 'universal',
  env: {
    baseUrl: process.env.NODE_ENV === 'production' ?
             `https://serene-depths-70253.herokuapp.com/api` :
             `http://${host}:${port}/api`
  },
  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: ['normalize.css', '~/assets/style/app.styl'],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/babel-polyfill',
    { src: '@/plugins/whatwg-fetch', ssr: false },
    '@/plugins/vuetify',
    '@/plugins/detect-browser',
    '@/plugins/axios',
    '@/plugins/pouchdb',
    '@/plugins/cuid'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
        config.devtool = 'eval-source-map';
      }
    }
  }
};
