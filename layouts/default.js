export default {
  data() {
    return {
      isNotSupported: !this.$isSupportedBrowser()
    };
  }
};
