export default {
  name: 'NewModel',
  data() {
    const dateFormatted = this.setAutoDate();

    return {
      valid: true,
      isLoading: false,
      date: this.parseDate(dateFormatted),
      dateRules: [
        v => !!v || 'Date is required'
      ],
      dateFormatted,
      isPickerActive: false,
      word: '',
      wordRules: [
        v => !!v || 'Word is required'
      ],
      translate: '',
      translateRules: [
        v => !!v || 'Translate is required'
      ],
      description: '',
      descriptionRules: [
        v => !!v || 'Description is required'
      ],
    };
  },

  watch: {
    date() {
      this.dateFormatted = this.formatDate(this.date);
    }
  },

  methods: {
    setAutoDate() {
      let date = new Date();
      let month = date.getMonth() + 1;
      month = month < 10 ? `0${month}` : month;
      let day = date.getDate();
      day = day < 10 ? `0${day}` : day;
      return `${month}/${day}/${date.getFullYear()}`;
    },

    formatDate(date) {
      if (!date) return null;

      const [year, month, day] = date.split('-');
      return `${month}/${day}/${year}`;
    },

    parseDate(date) {
      if (!date) return null;

      const [month, day, year] = date.split('/');
      return `${year}-${month.padStart(2, '0')}-${day.padStart(2, '0')}`;
    },

    submit() {
      if (this.$refs.form.validate()) {
        const {db} = this.$pouchdbApi.databases.words;
        const word = {
          _id: 'word:' + this.$cuid(),
          type: 'word',
          createdAt: new Date().toISOString(),
          updatedAt: new Date().toISOString(),
          date: this.date,
          word: this.word,
          translate: this.translate,
          description: this.description
        };

        // write to database
        db.put(word);

        // clear form
        this.$refs.form.reset();
      }
    }
  }
};
