import WordAssoc from '~/components/WordAssoc/WordAssoc.vue';

export default {
  components: {
    WordAssoc
  },

  data: () => ({
    words: [],
    syncStatus: 'notsyncing'
  }),

  /**
   * Called once when the app is first loaded
   */
  created() {
    const { db, url } = this.$pouchdbApi.databases.words;

    this.saveLocalDoc({
      '_id': '_local/user',
      'syncURL': url
    });

    // create database index on 'type'
    db.createIndex({index: {fields: ['type']}}).then(() => {
      // load all 'word' items
      const q = {
        selector: {
          type: 'word'
        }
      };
      return db.find(q);
    }).then((data) => {
      // write the words items to the Vue model
      this.words = data.docs;

      // load settings (Cloudant sync URL)
      return db.get('_local/user');
    }).then(() => {
      // if we have settings, start syncing
      this.startSync();
    }).catch((e) => {
    });
  },

  methods: {
    /**
     * Saves 'doc' to PouchDB. It first checks whether that doc
     * exists in the database. If it does, it overwrites it - if
     * it doesn't, it just writes it.
     * @param {Object} doc
     * @returns {Promise}
     */
    saveLocalDoc(doc) {
      const { db } = this.$pouchdbApi.databases.words;
      return db.get(doc._id).then((data) => {
        doc._rev = data._rev;
        return db.put(doc);
      }).catch((e) => {
        return db.put(doc);
      });
    },

    /**
     * Called when the sync process is to start. Initiates a PouchDB to
     * to Cloudant two-way sync and listens to the changes coming in
     * from the Cloudant feed. We need to monitor the incoming change
     * so that the Vue.js model is kept in sync.
     */
    startSync() {
      const { db, url } = this.$pouchdbApi.databases.words;

      this.syncStatus = 'notsyncing';
      if (this.sync) {
        this.sync.cancel();
        this.sync = null;
      }
      if (!url) { return; }
      this.syncStatus = 'syncing';
      this.sync = db.sync(url, {
        live: true,
        retry: false
      }).on('change', (info) => {
        // handle change
        // if this is an incoming change
        if (info.direction === 'pull' && info.change && info.change.docs) {
          info.change.docs.forEach((change) => {
            let arr = null;
            if (change._id.match(/^word/)) {
              arr = this.words;
            }
            else {
              return false;
            }

            const index = this.findDoc(arr, change._id);
            if (index > -1) {
              // and it's a deletion
              if (change._deleted) {
                // remove it
                arr.splice(index, 1);
              }
              else {
                // modify it
                delete change._revisions;
                Vue.set(arr, index, change);
              }
            }
            else {
              // add it
              if (!change._deleted) {
                arr.push(change);
              }
            }
          });
        }
      }).on('error', (e) => {
        this.syncStatus = 'syncerror';
      }).on('denied', (e) => {
        this.syncStatus = 'syncerror';
      }).on('paused', (e) => {
        if (e) {
          this.syncStatus = 'syncerror';
        }
      });
    },

    /**
     * Given a list of docs and an id, find the doc in the list that has
     * an '_id' (key) that matches the incoming id. Returns an object
     * with the
     *   i - the index where the item was found
     *   doc - the matching document
     * @param {Array} docs
     * @param {String} id
     * @param {String} key
     * @returns {Object}
     */
    findDoc: function (docs, id, key = '_id') {
      return docs.findIndex((item) => item[key] === id);
    }
  },

  asyncData({app, client}, callback) {
    app.$axios.get(`/words/`)
      .then((res) => {
        callback(null, {words: res.data});
      });
  }
};
